import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Component.Identifier;

public class JInputTest {

	Controller gameController = null;
	boolean useGameController = false;
	
	Identifier leftY = null;
	Identifier rightY = null;
	int rpmLimitVal = 1000;

	
	public JInputTest() {
		// TODO Auto-generated method stub
		Controller[] ca = ControllerEnvironment.getDefaultEnvironment().getControllers();
		for (Controller c : ca) {
			System.out.println("name: " + c.getName());
			if (c.getName().equals("Wireless 360 Controller")) {
				gameController = c;
				useGameController = true;
				
				Component[] components = c.getComponents();
	            System.out.println("Component Count: "+components.length);
	            for(Component cmp : components){
	                
	                // Get the components name 
	                System.out.println("Component : "+ cmp.getName());
	                System.out.println("    Identifier: "+ cmp.getIdentifier().getName());
	                System.out.print("    ComponentType: ");
	                if (cmp.isRelative()) {
	                    System.out.print("Relative");
	                } else {
	                    System.out.print("Absolute");
	                }
	                if (cmp.isAnalog()) {
	                    System.out.print(" Analog");
		                System.out.print(System.lineSeparator() + "    Value: "+ cmp.getPollData());
	                } else {
	                    System.out.print(" Digital");
	                }
	                System.out.println();
	                
	                if (cmp.getName().equals("y")) {
	                	leftY = cmp.getIdentifier();
	                } else if (cmp.getName().equals("ry")) {
	                	rightY = cmp.getIdentifier();
	                }
	            }
			} else if (c.getName().equals("Wireless Controller")) {
				gameController = c;
				useGameController = true;
				
				Component[] components = c.getComponents();
	            System.out.println("Component Count: "+components.length);
	            for(Component cmp : components){
	                
	                // Get the components name 
	                System.out.println("Component : "+ cmp.getName());
	                System.out.println("    Identifier: "+ cmp.getIdentifier().getName());
	                System.out.print("    ComponentType: ");
	                if (cmp.isRelative()) {
	                    System.out.print("Relative");
	                } else {
	                    System.out.print("Absolute");
	                }
	                if (cmp.isAnalog()) {
	                    System.out.print(" Analog");
		                System.out.print(System.lineSeparator() + "    Value: "+ cmp.getPollData());
	                } else {
	                    System.out.print(" Digital");
	                }
	                System.out.println();
	                
	                if (cmp.getName().equals("y")) {
	                		leftY = cmp.getIdentifier();
	                } else if (cmp.getName().equals("rz")) {
	                		rightY = cmp.getIdentifier();
	                }
	            }
			}
		}
	}

	
	public static void main(String[] args) {
		JInputTest jit = new JInputTest();
	}

}
